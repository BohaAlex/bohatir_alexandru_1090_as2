const FIRST_NAME = "ALEXANDRU";
const LAST_NAME = "BOHATIR";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
function initCaching() {
   let returnedObj={
   	cache: {},
   	pageAccessCounter: function(par1){
   		if(par1!=undefined){   			
   			if(this.cache[par1])
   				this.cache[par1]++;
   			else
   				this.cache[par1]=1;
   		}
   		else
   		{
   			if(this.cache['home'])
   				this.cache['home']++;
   			else
   				this.cache['home']=1;
   		}
   	},
   	getCache: function(){
   		return this.cache;
   	}
   }

   return returnedObj;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

